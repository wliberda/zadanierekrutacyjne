package com.zadanie.application

import android.app.Application
import android.content.Context
import com.zadanie.data.DataManager
import com.zadanie.data.DataManagerImpl
import com.zadanie.di.NetworkModule
import com.zadanie.utils.SchedulerProvider
import com.zadanie.utils.SchedulerProviderImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

/**
 * Created by wojciechliberda on 06/11/2017.
 */
@Singleton
@Module(includes = arrayOf(NetworkModule::class))
abstract class ApplicationModule(private val app: Application){

    @Binds
    abstract fun bindContext(app: Application): Context

    @Binds
    abstract fun bindDataManager(dataManagerImpl: DataManagerImpl): DataManager

    @Binds
    abstract fun bindScheduler(schedulerProviderImpl: SchedulerProviderImpl): SchedulerProvider
}
