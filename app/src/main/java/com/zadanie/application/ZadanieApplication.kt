package com.zadanie.application

import android.app.Activity
import android.app.Application
import com.zadanie.BuildConfig
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by wojciechliberda on 06/11/2017.
 */
class ZadanieApplication : Application(), HasActivityInjector {
    @Inject
    lateinit var dispatchingActivityInjector: DispatchingAndroidInjector<Activity>

    override fun activityInjector(): AndroidInjector<Activity>  = dispatchingActivityInjector


    override fun onCreate() {
        super.onCreate()
        if(BuildConfig.DEBUG) Timber.plant(Timber.DebugTree())
        DaggerAppComponent.builder()
                .application(this)
                .build()
                .inject(this)

    }
}