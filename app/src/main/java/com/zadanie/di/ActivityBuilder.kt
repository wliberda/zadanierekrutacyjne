package com.zadanie.di

import com.zadanie.ui.main.MainActivity
import com.zadanie.ui.main.MainModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by wojciechliberda on 06/11/2017.
 */
@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = arrayOf(MainModule::class))
    abstract fun contributeMainActivity() : MainActivity

}