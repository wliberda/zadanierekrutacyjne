package com.zadanie.data.network

import com.google.android.gms.location.places.Place
import com.zadanie.model.PlaceList
import io.reactivex.Flowable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by wojciechliberda on 07/11/2017.
 */
interface RestApi {

    @GET("maps/api/place/nearbysearch/json")
    fun findPlaces(@Query("keyword") query: String,
                   @Query("location") location: String,
                   @Query("key") key: String,
                   @Query("radius") radius: Int) : Flowable<Response<PlaceList>>
}