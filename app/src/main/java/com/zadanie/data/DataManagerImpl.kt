package com.zadanie.data

import com.google.android.gms.location.places.Place
import com.zadanie.data.network.RestApi
import com.zadanie.model.PlaceList
import io.reactivex.Flowable
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by wojciechliberda on 07/11/2017.
 */
@Singleton
class DataManagerImpl @Inject constructor(private val restApi: RestApi) : DataManager, RestApi {

    override fun findPlaces(query: String, location: String, key: String, radius: Int): Flowable<Response<PlaceList>> =
            restApi.findPlaces(query, location, key, radius)

}