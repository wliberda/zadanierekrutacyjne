package com.zadanie.ui.base

import com.zadanie.data.DataManager
import com.zadanie.mvp.BaseMVP
import com.zadanie.utils.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * Created by wojciechliberda on 06/11/2017.
 */
abstract class BasePresenter<out T: BaseMVP.BaseView>(val view: T,
                                                      val disposable: CompositeDisposable,
                                                      val schedulerProvider: SchedulerProvider,
                                                      val dataManager: DataManager) : BaseMVP.BasePresenter {

    override fun onStop() {
        disposable.clear()
    }
}