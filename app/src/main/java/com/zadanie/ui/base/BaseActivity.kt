package com.zadanie.ui.base

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.zadanie.application.AppComponent
import com.zadanie.mvp.BaseMVP
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

/**
 * Created by wojciechliberda on 06/11/2017.
 */
abstract class BaseActivity < T: BaseMVP.BasePresenter>: AppCompatActivity(), BaseMVP.BaseView, HasSupportFragmentInjector{

    @Inject
    lateinit var dispatchinFragmentInjector : DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var presenter: T

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = dispatchinFragmentInjector

    abstract fun getLayoutId(): Int
    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(getLayoutId())
    }

    override fun onStop() {
        super.onStop()
        presenter.onStop()
    }

}
