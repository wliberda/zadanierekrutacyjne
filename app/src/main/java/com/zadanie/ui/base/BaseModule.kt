package com.zadanie.ui.base

import dagger.Binds
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * Created by wojciechliberda on 06/11/2017.
 */
@Module
 class BaseModule {

    @Provides
    fun provideDisposable(): CompositeDisposable = CompositeDisposable()
}