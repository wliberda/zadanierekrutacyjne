package com.zadanie.ui.main

import com.zadanie.data.DataManager
import com.zadanie.model.Place
import com.zadanie.mvp.MainMVP
import com.zadanie.ui.base.BasePresenter
import com.zadanie.utils.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber
import javax.inject.Inject


 class MainPresenter @Inject constructor(view: MainMVP.View,
                                        disposable: CompositeDisposable,
                                        schedulerProvider: SchedulerProvider,
                                        dataManager: DataManager)
    : BasePresenter<MainMVP.View>(view, disposable, schedulerProvider, dataManager), MainMVP.Presenter {
    override fun mapReady() {
        when(view.checkLocalPermission()){
            true -> view.getCurrentLocation()
            false -> view.requestLocalPermission()
        }
    }

    override fun locationPermissionResult(isGranted: Boolean) {
        when(isGranted){
            true -> view.getCurrentLocation()
            false -> view.showErrorNoPermission()

        }
    }

     override fun locationIsNull() {
        if(!view.checkLocalPermission()) {
            view.requestLocalPermission()
        } else view.getCurrentLocation()
     }

     override fun queryPassed(query: String, location: String, key: String, isNetworkAvailable: Boolean) {
        when{
            !isNetworkAvailable -> view.showErrorNoInternet()
            query != "" && location != "" -> {
                view.clearMap()
                view.clearRecyclerView()
                view.getCurrentLocation()
                disposable.add(dataManager.findPlaces(query, location, key,10000)
                        .subscribeOn(schedulerProvider.io())
                        .flatMapIterable { it.body()?.results }
                        .take(3)
                        .observeOn(schedulerProvider.ui())
                        .subscribe({
                            view.addToRecyclerView(it)
                            view.addOnMap(location = it.geometry?.location!!, id = it.id!!)
                        },{
                            Timber.e("Error ${it.message}")
                        })
                )
            }
        }
    }

     override fun itemClicked(place: Place?) {
         place?.let {
             view.focusOnMarker(it.id!!)
         }
     }
 }