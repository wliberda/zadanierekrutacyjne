package com.zadanie.ui.main

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.zadanie.R
import com.zadanie.model.Place
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject


class PlaceMainRecyclerViewAdapter(val context: Context) : RecyclerView.Adapter<PlaceMainRecyclerViewAdapter.ViewHolder>() {
    private val list: MutableList<Place> = mutableListOf()
    private val onClickSubject = PublishSubject.create<Place>()

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        setValues(holder, position)
    }

    private fun setValues(holder: ViewHolder?, position: Int) {
        holder?.apply {
            list[position].icon?.let { url ->
                Picasso.with(context)
                        .load(url)
                        .into(ivIcon)
            }
            tvName.text = list[position].name
            list[position].opening_hours?.open_now?.let {
                tvOpeningNow.text = when(it){
                    true -> context.resources.getString(R.string.open_now)
                    false -> context.resources.getString(R.string.close_now)
                }
            }
            tvAddress.text = list[position].vicinity
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder =
            ViewHolder(LayoutInflater.from(parent!!.context).inflate(R.layout.place_adapter,parent, false))

    override fun getItemCount(): Int = list.size

    fun addItem(place: Place) {
        list.add(place)
        notifyItemInserted(list.size - 1)
    }

    fun removeList(){
        val size = list.size
        list.clear()
        notifyItemRangeRemoved(0, size)
    }

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val tvName = view.findViewById<TextView>(R.id.tvName)
        val ivIcon = view.findViewById<ImageView>(R.id.ivIcon)
        val tvOpeningNow = view.findViewById<TextView>(R.id.tvOpeningNow)
        val tvAddress = view.findViewById<TextView>(R.id.tvAddress)
        init {
            itemView.setOnClickListener { onClickSubject.onNext(list[adapterPosition])}
        }
    }


    val positionClick: Observable<Place>
        get() = onClickSubject

}