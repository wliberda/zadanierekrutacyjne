package com.zadanie.ui.main

import com.zadanie.mvp.MainMVP
import com.zadanie.ui.base.BaseModule
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Created by wojciechliberda on 06/11/2017.
 */
@Module(includes = arrayOf(BaseModule::class))
abstract class MainModule {

    @Binds
    abstract fun provideView(activity: MainActivity) : MainMVP.View

    @Binds
    abstract fun providePresenter(presenter: MainPresenter): MainMVP.Presenter




}