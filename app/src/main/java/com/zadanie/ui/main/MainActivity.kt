package com.zadanie.ui.main

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.places.GeoDataClient
import com.google.android.gms.location.places.PlaceDetectionClient
import com.google.android.gms.location.places.Places
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.tasks.Task
import com.zadanie.R
import com.zadanie.model.Place
import com.zadanie.mvp.MainMVP
import com.zadanie.ui.base.BaseActivity
import com.zadanie.utils.Boast
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber


class MainActivity : BaseActivity<MainMVP.Presenter>() ,
        MainMVP.View,
        OnMapReadyCallback {
    private var lastLocation: Location? = null
    private val REQUEST_LOCATION_PERMISSION = 101
    private var map: GoogleMap? = null
    private var locationMarker: Marker? = null
    private var listOfMarkers = mutableListOf<Marker>()
    private var rvAdapter: PlaceMainRecyclerViewAdapter? = null
    private var mapFragment: SupportMapFragment? = null
    private var geoDataClient: GeoDataClient? = null
    private var placeDetectionClient: PlaceDetectionClient? = null
    private var fusedLocationProviderClient: FusedLocationProviderClient? = null



    override fun getLayoutId(): Int = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        geoDataClient = Places.getGeoDataClient(this, null)
        placeDetectionClient = Places.getPlaceDetectionClient(this, null)
        fusedLocationProviderClient =  LocationServices.getFusedLocationProviderClient(this)
        mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment!!.getMapAsync(this)
        setSearchView()
        setRecyclerView()
    }

    private fun setSearchView() {
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                query?.let { query ->
                    if(lastLocation == null) presenter.locationIsNull()
                    lastLocation?.let { location ->
                        val locationString = "${location.latitude},${location.longitude}"
                        presenter.queryPassed(query = query,
                                location = locationString,
                                key = resources.getString(R.string.api_key),
                                isNetworkAvailable = isNetworkConnected)
                    }
                }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean = false
        })
    }

    private fun setRecyclerView() {
        rvAdapter = PlaceMainRecyclerViewAdapter(context = this)
        recyclerView.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            addItemDecoration(DividerItemDecoration(this@MainActivity, DividerItemDecoration.VERTICAL))
            adapter = rvAdapter
            rvAdapter?.positionClick?.subscribe {
                presenter.itemClicked(it)
            }
        }
    }

    override fun onMapReady(map: GoogleMap?) {
        map?.let {
            this.map = map
            presenter.mapReady()
        }

    }

    override fun checkLocalPermission():Boolean =
        ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED

    override fun requestLocalPermission() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_LOCATION_PERMISSION)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode){
            REQUEST_LOCATION_PERMISSION ->
                    presenter.locationPermissionResult(
                            isGranted =  grantResults[0] == PackageManager.PERMISSION_GRANTED
                            )
        }
    }

    @SuppressLint("MissingPermission")
    override fun getCurrentLocation() {
        try{
            val locationResult = fusedLocationProviderClient!!.lastLocation as Task<Location>
            locationResult.addOnCompleteListener {
                when(it.isSuccessful){
                    true -> {
                        it.result?.let {
                            lastLocation = it
                            lastLocation?.let {
                                locationMarker?.remove()
                                locationMarker = map?.addMarker(
                                        MarkerOptions()
                                                .position(LatLng(it.latitude, it.longitude))
                                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)))
                                map?.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(it.latitude, it.longitude), 10f))
                            }
                        }
                    }
                    false -> {
                        Timber.e("Error")
                    }
                }
            }
        }catch (e: SecurityException) {
            Timber.e("Exception ${e.message}")
        }

    }

    override fun clearRecyclerView() {
        rvAdapter?.removeList()
    }

    override fun clearMap() {
        listOfMarkers.forEach { it.remove() }
    }

    override fun addOnMap(location: com.zadanie.model.Location, id: String) {
        val marker = map?.addMarker(MarkerOptions().position(LatLng(location.lat, location.lng)))
        marker!!.tag = id
        listOfMarkers.add(marker)
    }

    override fun showErrorNoPermission() {
        Boast.makeText(this, resources.getString(R.string.error_no_permission)).show()
    }

    override fun showErrorNoInternet() {
        Boast.makeText(this, resources.getString(R.string.error_no_internet)).show()
    }

    override fun addToRecyclerView(place: Place) {
        rvAdapter?.addItem(place)
    }

    override fun focusOnMarker(id: String) {

        listOfMarkers.first{ it.tag == id}.let { marker ->
            map?.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(marker.position.latitude, marker.position.longitude), 12f))
        }
    }

    internal val isNetworkConnected: Boolean
        get() {
            val connMgr = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networkInfo = connMgr.activeNetworkInfo
            return networkInfo != null && networkInfo.isConnected
        }


}