package com.zadanie.mvp

/**
 * Created by wojciechliberda on 06/11/2017.
 */
interface BaseMVP {

    interface BaseView {

    }

    interface BasePresenter {
        fun onStop()
    }

}