package com.zadanie.mvp

import com.zadanie.model.Location
import com.zadanie.model.Place

/**
 * Created by wojciechliberda on 06/11/2017.
 */
interface MainMVP {
    interface View : BaseMVP.BaseView {
        fun checkLocalPermission() : Boolean
        fun requestLocalPermission()
        fun getCurrentLocation()
        fun showErrorNoPermission()
        fun showErrorNoInternet()
        fun addOnMap(location: Location, id: String)
        fun clearMap()
        fun addToRecyclerView(place: Place)
        fun clearRecyclerView()
        fun focusOnMarker(id: String)
    }


    interface Presenter: BaseMVP.BasePresenter {
        fun mapReady()
        fun locationPermissionResult(isGranted: Boolean)
        fun queryPassed(query: String, location: String, key: String, isNetworkAvailable: Boolean)
        fun locationIsNull()
        fun itemClicked(place: Place?)
    }
}