package com.zadanie.utils

import io.reactivex.Scheduler

/**
 * Created by wojciechliberda on 07/11/2017.
 */
interface SchedulerProvider {
    fun ui(): Scheduler
    fun io(): Scheduler
    fun computation(): Scheduler

}