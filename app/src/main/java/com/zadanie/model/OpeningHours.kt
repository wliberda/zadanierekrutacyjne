package com.zadanie.model

/**
 * Created by wojciechliberda on 07/11/2017.
 */
data class OpeningHours(val open_now: Boolean, val weekday_text: List<String>)