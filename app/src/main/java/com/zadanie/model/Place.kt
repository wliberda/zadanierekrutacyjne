package com.zadanie.model

/**
 * Created by wojciechliberda on 07/11/2017.
 */
data class Place(
        val name: String?,
        val geometry: Geometry?,
        val icon: String?,
        val id: String?,
        val photos: List<Photo>?,
        val place_id: String?,
        val price_level: Int?,
        val rating: Double?,
        val reference: String?,
        val opening_hours: OpeningHours?,
        val types: List<String>?,
        val vicinity: String?
)