package com.zadanie.model

/**
 * Created by wojciechliberda on 07/11/2017.
 */
data class Viewport(val northeast: Northeast, val southwest: Southwest)