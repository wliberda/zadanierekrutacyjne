package com.zadanie.model

/**
 * Created by wojciechliberda on 07/11/2017.
 */
data class Photo(val height: Int, val width: Int, val photo_reference: String, val html_attributions: List<String>)