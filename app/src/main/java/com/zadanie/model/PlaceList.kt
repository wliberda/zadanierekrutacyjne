package com.zadanie.model


/**
 * Created by wojciechliberda on 07/11/2017.
 */
data class PlaceList(val results: List<Place>) {

}