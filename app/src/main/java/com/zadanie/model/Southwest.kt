package com.zadanie.model

/**
 * Created by wojciechliberda on 07/11/2017.
 */
data class Southwest(val lat: Double, val lng: Double)