package com.zadanie.ui.main

import com.zadanie.TestSchedulerProvider
import com.zadanie.data.DataManager
import com.zadanie.model.Geometry
import com.zadanie.model.Location
import com.zadanie.model.Place
import com.zadanie.model.PlaceList
import com.zadanie.mvp.MainMVP
import io.reactivex.Flowable
import io.reactivex.disposables.CompositeDisposable
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Response


@RunWith(MockitoJUnitRunner::class)
class MainPresenterTest{

    @Mock
    lateinit var view: MainMVP.View

    @Mock
    lateinit var dataManager: DataManager

    private fun <T> any(): T {
        Mockito.any<T>()
        return uninitialized()
    }
    private fun <T> uninitialized(): T = null as T

    var presenter: MainPresenter? = null

    @Before
    fun setUp(){
        presenter = MainPresenter(view, CompositeDisposable(),TestSchedulerProvider(), dataManager )
    }

    @Test
    fun presenterShouldNotBeNull(){
        assertNotNull(presenter)
    }


    @Test
    fun shouldCheckLocationPermissionWhenMapIsRead(){
        presenter!!.mapReady()
        verify(view).checkLocalPermission()
    }

    @Test
    fun shouldRequestPermissionWhenNotPermitted(){
        `when`(view.checkLocalPermission()).thenReturn(false)
        presenter!!.mapReady()
        verify(view).requestLocalPermission()
    }

    @Test
    fun shouldGetCurrentLocationWhenPermissionIsGranted(){
        `when`(view.checkLocalPermission()).thenReturn(true)
        presenter!!.mapReady()
        verify(view).getCurrentLocation()
        verify(view, never()).requestLocalPermission()
    }


    @Test
    fun shouldShowPermissionErrorWhenNotGranted(){
        presenter!!.locationPermissionResult(false)
        verify(view).showErrorNoPermission()
        verify(view, never()).getCurrentLocation()
    }

    @Test
    fun shouldGetLocationAfterPermissionGranted(){
        presenter!!.locationPermissionResult(true)
        verify(view,never()).showErrorNoPermission()
        verify(view).getCurrentLocation()
    }


    @Test
    fun shouldShowErrorWhenInternetNotAvailable(){
        presenter!!.queryPassed("query", "key", "key", false)
        verify(dataManager, never()).findPlaces(ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.anyInt())
        verify(view).showErrorNoInternet()
    }

    @Test
    fun shouldCheckLocalPermissionWhenNullLocation(){
        `when`(view.checkLocalPermission()).thenReturn(false)
        presenter!!.locationIsNull()
        verify(view).requestLocalPermission()
    }

    @Test
    fun shouldGetLocationWhenLocatioIsNullAndPermissionIsGranted(){
        `when`(view.checkLocalPermission()).thenReturn(true)
        presenter!!.locationIsNull()
        verify(view).getCurrentLocation()
        verify(view, never()).requestLocalPermission()
    }

    @Test
    fun shouldGetPlaceWhenQueryPassed(){
        val query = "restauracja"
        val locationString = "23.2323,12.3424"
        val placeList = PlaceList(mutableListOf())
        `when`(dataManager.findPlaces(ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.anyInt())).thenReturn(Flowable.fromCallable { Response.success(placeList) })
        presenter!!.queryPassed(query, locationString, "key", true)
        verify(view).clearMap()
        verify(view).clearRecyclerView()
        verify(dataManager).findPlaces(ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), any(), ArgumentMatchers.anyInt())
    }


    @Test
    fun shouldAddThreeMarkersOnMap(){
        val place = Place("name", Geometry(Location(23.12,44.21), null),null,"id",null, null, null,null,null,null,null, null)
        val query = "restauracja"
        val locationString = "23.2323,12.3424"
        val placeList = PlaceList(mutableListOf(place, place, place, place, place))
        `when`(dataManager.findPlaces(ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.anyInt())).thenReturn(Flowable.fromCallable { Response.success(placeList) })
        presenter!!.queryPassed(query, locationString, "key", true)
        verify(view).clearMap()
        verify(view).clearRecyclerView()
        verify(dataManager).findPlaces(ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), any(), ArgumentMatchers.anyInt())
        verify(view, times(3)).addOnMap(any(), ArgumentMatchers.anyString())
    }


    @Test
    fun shouldFocusOnMarkerWhenItemCkicked(){
        val place = Place("name", Geometry(Location(23.12,44.21), null),null,"id",null, null, null,null,null,null,null, null)
        presenter!!.itemClicked(place)
        verify(view).focusOnMarker(place.id!!)
    }


}